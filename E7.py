# Autor_ Angel Marcelo Merchan Infante
# Email_ angel.merchan@unl.edu.ec

# Read a string:
# s = input()
# Print a value:
# print(s)

s = int(input())
diccionario = {}
for _ in range(s):
  for palabra in input().split():
    diccionario[palabra] = diccionario.get(palabra, 0) + 1

frecuenciaPa = [(-diccionario, palabra) for (palabra, diccionario) in diccionario.items()]
for c, palabra in sorted(frecuenciaPa):
     print(palabra,c)