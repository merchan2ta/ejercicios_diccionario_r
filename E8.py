# Autor_ Angel Marcelo Merchan Infante
# Email_ angel.merchan@unl.edu.ec

# Read a string:
# s = input()
# Print a value:
# print(s)


lat_in = {}

for _ in range(int(input())):
  eng, *latin = input().replace('- ', '').replace(',', '').split()
  for la_word in latin:
    if la_word not in lat_in.keys():
      lat_in[la_word] = []
    lat_in[la_word].append(eng)

print()
print(len(lat_in))
for w in sorted(lat_in.keys()):
  print(w, '-', ', '.join(sorted(list(set(lat_in[w])))))