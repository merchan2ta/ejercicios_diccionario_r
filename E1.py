# Autor_ Angel Marcelo Merchan Infante
# Email_ angel.merchan@unl.edu.ec
#Instruccions__The text is given in a single line. For each word of the text count the number
# of its occurrences before it.

# Read a string:
# s = input()
# Print a value:
# print(s)

s = input().split()
frecuencia = {}
for palabra in s :
  if palabra not in frecuencia:
    frecuencia[palabra] = 0
  print(frecuencia[palabra], end=' ')
  frecuencia[palabra] += 1
